package ru.t1.artamonov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskGetByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    public TaskGetByIdRequest(@Nullable String token) {
        super(token);
    }

    public TaskGetByIdRequest(@Nullable String token, @Nullable String taskId) {
        super(token);
        this.taskId = taskId;
    }

}
