package ru.t1.artamonov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    private TaskDTO task;

    public AbstractTaskResponse(@Nullable final TaskDTO task) {
        this.task = task;
    }

}
