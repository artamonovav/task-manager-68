package ru.t1.artamonov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRemoveByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    public TaskRemoveByIdRequest(@Nullable String token) {
        super(token);
    }

    public TaskRemoveByIdRequest(@Nullable String token, @Nullable String taskId) {
        super(token);
        this.taskId = taskId;
    }

}

