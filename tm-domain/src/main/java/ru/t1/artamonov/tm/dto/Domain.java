package ru.t1.artamonov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.dto.model.ProjectDTO;
import ru.t1.artamonov.tm.dto.model.TaskDTO;
import ru.t1.artamonov.tm.dto.model.UserDTO;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@XmlRootElement
@NoArgsConstructor
public final class Domain implements Serializable {

    private static final long serialVersionUID = 1;

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private Date created = new Date();

    @NotNull
    private List<UserDTO> users = new ArrayList<>();

    @NotNull
    private List<ProjectDTO> projects = new ArrayList<>();

    @NotNull
    private List<TaskDTO> tasks = new ArrayList<>();

}
