package ru.t1.artamonov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskCreateRequest(
            @Nullable String token
    ) {
        super(token);
    }

    public TaskCreateRequest(
            @Nullable String token,
            @Nullable String name,
            @Nullable String description
    ) {
        super(token);
        this.name = name;
        this.description = description;
    }

}
