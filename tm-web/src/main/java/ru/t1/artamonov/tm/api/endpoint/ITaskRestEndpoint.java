package ru.t1.artamonov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.Task;

import java.util.Collection;

public interface ITaskRestEndpoint {

    void delete(Task task);

    void delete(String id);

    @Nullable
    Collection<Task> findAll();

    @Nullable
    Task findById(String id);

    @NotNull
    Task save(Task task);

}
