package ru.t1.artamonov.tm;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.artamonov.tm.config.ApplicationConfiguration;
import ru.t1.artamonov.tm.config.DatabaseConfiguration;
import ru.t1.artamonov.tm.config.WebApplicationConfiguration;
import ru.t1.artamonov.tm.config.WebConfig;

import javax.servlet.ServletContext;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ApplicationConfiguration.class, DatabaseConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebApplicationConfiguration.class, WebConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected void registerContextLoaderListener(ServletContext servletContext) {
        super.registerContextLoaderListener(servletContext);
    }

}
