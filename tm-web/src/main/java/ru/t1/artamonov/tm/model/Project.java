package ru.t1.artamonov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.artamonov.tm.enumerated.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "project", propOrder = {
        "id",
        "name",
        "description",
        "status",
        "dateStart",
        "dateFinish"
})
public class Project {

    @Id
    @NotNull
    @XmlElement
    private String id = UUID.randomUUID().toString();

    @Column
    @Nullable
    @XmlElement(required = true)
    private String name;

    @Column
    @Nullable
    @XmlElement
    private String description;

    @Column
    @NotNull
    @XmlElement
    @Enumerated(EnumType.STRING)
    @XmlSchemaType(name = "string")
    private Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    @XmlElement
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart = new Date();

    @Column
    @Nullable
    @XmlElement
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    public Project(@NotNull final String name) {
        this.name = name;
    }

}
