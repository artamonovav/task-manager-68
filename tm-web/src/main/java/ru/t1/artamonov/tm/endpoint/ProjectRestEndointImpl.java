package ru.t1.artamonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.artamonov.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.artamonov.tm.model.Project;
import ru.t1.artamonov.tm.service.ProjectService;

import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndointImpl implements IProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody final Project project) {
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void delete(@PathVariable("id") String id) {
        projectService.removeById(id);
    }

    @Override
    @GetMapping("/findAll")
    public @Nullable Collection<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @GetMapping("/findById/{id}")
    public @Nullable Project findById(@PathVariable("id") String id) {
        return projectService.findOneById(id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public Project save(@RequestBody final Project project) {
        projectService.add(project);
        return project;
    }

}
