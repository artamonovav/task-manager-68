package ru.t1.artamonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.artamonov.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.artamonov.tm.model.Task;
import ru.t1.artamonov.tm.service.TaskService;

import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpointImpl implements ITaskRestEndpoint {

    @NotNull
    @Autowired
    private TaskService taskService;

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody Task task) {
        taskService.remove(task);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void delete(@PathVariable("id") String id) {
        taskService.removeById(id);
    }

    @Nullable
    @Override
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return taskService.findAll();
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public Task findById(@PathVariable("id") String id) {
        return taskService.findOneById(id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public Task save(@RequestBody Task task) {
        taskService.add(task);
        return task;
    }

}
