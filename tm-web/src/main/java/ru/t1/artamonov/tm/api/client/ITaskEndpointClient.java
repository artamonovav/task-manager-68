package ru.t1.artamonov.tm.api.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.artamonov.tm.model.Task;

import java.util.Collection;

public interface ITaskEndpointClient {

    static ITaskEndpointClient taskClient(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ITaskEndpointClient.class, baseUrl);
    }

    @PostMapping(value = "/delete", produces = "application/json")
    void delete(@RequestBody Task task);

    @PostMapping(value = "/deleteById/{id}", produces = "application/json")
    void delete(@PathVariable("id") String id);

    @GetMapping(value = "/findAll", produces = "application/json")
    Collection<Task> findAll();

    @GetMapping(value = "/findById/{id}", produces = "application/json")
    Task findById(@PathVariable("id") String id);

    @PostMapping(value = "/save", produces = "application/json")
    void save(@RequestBody Task task);

}
