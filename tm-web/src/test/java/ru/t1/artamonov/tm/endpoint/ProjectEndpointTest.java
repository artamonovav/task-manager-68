package ru.t1.artamonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.client.IProjectEndpointClient;
import ru.t1.artamonov.tm.marker.IntegrationCategory;
import ru.t1.artamonov.tm.model.Project;

import java.util.Collection;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    final static String BASE_URL = "http://localhost:8080/api/projects";

    @NotNull
    final IProjectEndpointClient projectClient = IProjectEndpointClient.projectClient(BASE_URL);

    @NotNull
    final Project project1 = new Project("Project1");

    @NotNull
    final Project project2 = new Project("Project2");

    @NotNull
    final Project project3 = new Project("Project3");

    @Before
    public void init() {
        projectClient.save(project1);
        projectClient.save(project2);
    }

    @After
    public void clean() {
        projectClient.delete(project1);
        projectClient.delete(project2);
        projectClient.delete(project3);
    }

    @Test
    public void findAll() {
        Collection<Project> projects = projectClient.findAll();
        Assert.assertTrue(projects.size() >= 2);
    }

    @Test
    public void findById() {
        @NotNull final Project prj = projectClient.findById(project1.getId());
        Assert.assertNotNull(prj);
        Assert.assertEquals(prj.getId(), project1.getId());
    }

    @Test
    public void delete() {
        Assert.assertNotNull(projectClient.findById(project1.getId()));
        projectClient.delete(project1);
        Assert.assertNull(projectClient.findById(project1.getId()));
    }

    @Test
    public void deleteById() {
        Assert.assertNotNull(projectClient.findById(project2.getId()));
        projectClient.delete(project2);
        Assert.assertNull(projectClient.findById(project2.getId()));
    }

    @Test
    public void save() {
        projectClient.save(project3);
        Assert.assertEquals(project3.getId(), projectClient.findById(project3.getId()).getId());
    }

}
