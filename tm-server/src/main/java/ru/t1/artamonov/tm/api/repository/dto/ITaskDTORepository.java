package ru.t1.artamonov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.artamonov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDTORepository extends IUserOwnedDTORepository<TaskDTO> {

    long countByUserId(String userId);

    @Transactional
    void deleteByUserId(String userId);

    @Transactional
    void deleteByProjectId(String projectId);

    @Transactional
    void deleteByUserIdAndId(String userId, String id);

    boolean existsByUserIdAndId(String userId, String id);

    @Nullable
    List<TaskDTO> findAllByUserId(String userId);

    @Nullable
    List<TaskDTO> findAllByUserIdAndProjectId(String userId, String projectId);

    @Nullable
    TaskDTO findByUserIdAndId(String userId, String id);

    @Nullable
    TaskDTO findOneByUserIdAndId(String userId, String taskId);

    @Nullable
    @Query("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY :sortType")
    List<TaskDTO> findAllByUserIdWithSort(@NotNull @Param("userId") String userId, @NotNull @Param("sortType") String sortType);

}
