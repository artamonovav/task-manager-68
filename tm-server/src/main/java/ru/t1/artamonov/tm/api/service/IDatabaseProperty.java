package ru.t1.artamonov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseUsername();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHbm2DdlAuto();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getDatabaseFormatSql();

    @NotNull
    String getDatabaseSecondLevelCache();

    @NotNull
    String getDatabaseFactoryClass();

    @NotNull
    String getDatabaseUseQueryCash();

    @NotNull
    String getDatabaseUseMinPuts();

    @NotNull
    String getDatabaseRegionPrefix();

    @NotNull
    String getDatabaseConfigFilePath();

    @NotNull
    String getBrokerUrl();

    @NotNull
    String getBrokerQueue();


}
