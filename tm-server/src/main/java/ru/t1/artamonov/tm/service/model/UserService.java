package ru.t1.artamonov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.artamonov.tm.api.repository.model.IUserRepository;
import ru.t1.artamonov.tm.api.service.IPropertyService;
import ru.t1.artamonov.tm.api.service.model.IUserService;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.exception.entity.UserNotFoundException;
import ru.t1.artamonov.tm.exception.field.EmailEmptyException;
import ru.t1.artamonov.tm.exception.field.IdEmptyException;
import ru.t1.artamonov.tm.exception.field.LoginEmptyException;
import ru.t1.artamonov.tm.exception.field.PasswordEmptyException;
import ru.t1.artamonov.tm.exception.user.ExistsEmailException;
import ru.t1.artamonov.tm.exception.user.ExistsLoginException;
import ru.t1.artamonov.tm.model.User;
import ru.t1.artamonov.tm.util.HashUtil;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService implements IUserService {

    @NotNull
    @Autowired
    private final IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User create(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        boolean loginExist = isLoginExist(login);
        if (loginExist) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull User user = new User();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        user.setRole(Role.USUAL);
        userRepository.saveAndFlush(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User create(@Nullable String login, @Nullable String password, @Nullable String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        boolean loginExist = isLoginExist(login);
        if (loginExist) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        user.setRole(Role.USUAL);
        userRepository.saveAndFlush(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User create(@Nullable String login, @Nullable String password, @Nullable Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        boolean loginExist = isLoginExist(login);
        if (loginExist) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull User user = new User();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        if (role == null) user.setRole(Role.USUAL);
        else user.setRole(role);
        userRepository.saveAndFlush(user);
        return user;
    }

    @Override
    public boolean existsById(@Nullable String id) {
        return userRepository.existsById(id);
    }

    @Nullable
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    public User findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return userRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isLoginExist(@Nullable String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@Nullable String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    @SneakyThrows
    @Transactional
    public @NotNull User remove(@Nullable User user) {
        if (user == null) throw new UserNotFoundException();
        return removeById(user.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public @NotNull User removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        userRepository.delete(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User removeByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        userRepository.delete(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User removeByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        userRepository.delete(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<User> set(@NotNull Collection<User> users) {
        if (users == null) return new ArrayList<>();
        for (@NotNull User user : users)
            userRepository.saveAndFlush(user);
        return users;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User setPassword(@Nullable String id, @Nullable String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        userRepository.saveAndFlush(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        userRepository.saveAndFlush(user);
        return user;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void lockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        userRepository.saveAndFlush(user);

    }

    @Override
    @SneakyThrows
    @Transactional
    public void unlockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        userRepository.saveAndFlush(user);
    }

}
