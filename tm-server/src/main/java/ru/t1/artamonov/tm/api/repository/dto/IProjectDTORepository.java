package ru.t1.artamonov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.artamonov.tm.dto.model.ProjectDTO;

import java.util.List;

@Repository
public interface IProjectDTORepository extends IUserOwnedDTORepository<ProjectDTO> {

    long countByUserId(String userId);

    @Transactional
    void deleteByUserId(String userId);

    @Transactional
    void deleteByUserIdAndId(String userId, String id);

    boolean existsByUserIdAndId(String userId, String id);

    @Nullable
    List<ProjectDTO> findAllByUserId(String userId);

    @Nullable
    ProjectDTO findByUserIdAndId(String userId, String id);

    @Nullable
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY :sortType")
    List<ProjectDTO> findAllByUserIdWithSort(@NotNull @Param("userId") String userId, @NotNull @Param("sortType") String sortType);

}