package ru.t1.artamonov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.TaskCreateRequest;
import ru.t1.artamonov.tm.event.ConsoleEvent;
import ru.t1.artamonov.tm.util.TerminalUtil;

@Component
public final class TaskCreateListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-create";

    @NotNull
    private static final String DESCRIPTION = "Create new task.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskCreateListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CREATE TASK]");
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @Nullable final TaskCreateRequest request = new TaskCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        taskEndpointClient.createTask(request);
    }

}
